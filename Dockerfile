FROM openjdk:8-jdk-alpine

ADD src src

COPY  app-one-0.0.1-SNAPSHOT.jar app-one-0.0.1-SNAPSHOT.jar

ENTRYPOINT ["nohup","java","-jar","/app-one-0.0.1-SNAPSHOT.jar","&"]